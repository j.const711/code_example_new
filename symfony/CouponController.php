<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Coupon;
use App\Form\CouponType;
use App\Geocoding\LocationFactory;
use App\Model\Location;
use App\Repository\CouponRepository;
use App\Repository\Filter\CouponLocationFilter;
use App\Security\Voter\CouponVoter;
use App\Service\Coupon\CouponService;
use App\Service\Coupon\RedeemService;
use App\Service\Coupon\SerializeService;
use App\Utility\ListUtility;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\InsufficientAuthenticationException;

class CouponController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var CouponRepository
     */
    protected $couponRepository;

    /**
     * @var CouponService
     */
    protected $couponService;

    /**
     * @var SerializeService
     */
    protected $serializeService;

    /**
     * @var Location
     */
    protected $locationCenter;

    /**
     * @var RedeemService
     */
    protected $redeemService;

    public function __construct(
        EntityManagerInterface $entityManager,
        CouponRepository $couponRepository,
        CouponService $couponService,
        SerializeService $serializeService,
        RedeemService $redeemService,
        ?string $locationCenter
    ) {
        $this->entityManager = $entityManager;
        $this->couponRepository = $couponRepository;
        $this->couponService = $couponService;
        $this->serializeService = $serializeService;
        $this->redeemService = $redeemService;
        $parts = explode(',', $locationCenter);
        $this->locationCenter = new Location((float) $parts[0], (float) $parts[1]);
    }

    /**
     * @Rest\QueryParam(name="id", requirements="([0-9,]+)", nullable=false, description="Filter coupons by id in CSV format")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $id = $paramFetcher->get('id');
        $id = ListUtility::intExplode(',', $id);
        $coupons = $this->couponRepository->findActiveById($id);

        return $this->handleView($this->view($this->serializeService->build($coupons)));
    }

    /**
     * @Rest\QueryParam(name="sort", requirements="([a-z,-]+)", nullable=true, description="Sort result by field(s)")
     * @Rest\QueryParam(name="limit", requirements="([\d]+)", nullable=true, description="Limit number of fields")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     */
    public function getPublishedAction(ParamFetcherInterface $paramFetcher)
    {
        $sort = $paramFetcher->get('sort', true);
        if (is_string($sort)) {
            $sort = explode(',', $sort);
        }
        $limit = $paramFetcher->get('limit', true);

        $couponCollection = $this->couponRepository->findActive($limit, $sort);

        return $this->handleView($this->view($this->serializeService->build($couponCollection)));
    }

    /**
     * @Rest\QueryParam(name="locationCenter", requirements="([0-9.-]+,[0-9.-]+)", nullable=true, description="Search coupons around this point")
     * @Rest\QueryParam(name="locationRadius", requirements="([0-9]+)", nullable=false, description="Limit search to locationCenter (in meter) around locationCenter")
     * @Rest\QueryParam(name="latestCouponsOnly", requirements="([0-1])", nullable=false, description="Only latest coupons")
     * @Rest\QueryParam(name="categories", requirements="([0-9,]+)", nullable=true, description="Restrict coupons to list of categories")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     */
    public function getFilterAction(ParamFetcherInterface $paramFetcher)
    {
        $locationCenter = $paramFetcher->get('locationCenter', true);
//        $locationRadius = (int)$paramFetcher->get('locationRadius', true);
        $categories = $paramFetcher->get('categories', true);
        $latestCouponsOnly = (bool) $paramFetcher->get('latestCouponsOnly', true);

        if (null !== $locationCenter) {
            $parts = explode(',', $locationCenter);
            $locationCenter = new Location((float) $parts[0], (float) $parts[1]);
        } else {
            $locationCenter = $this->locationCenter;
        }

        if (empty($categories)) {
            $categories = [];
        } else {
            $categories = explode(',', $categories);
            foreach ($categories as &$category) {
                $category = (int) $category;
            }
        }

        // Ignore locationRadius for now
        $locationRadius = 1000000;

        $couponLocationFilter = new CouponLocationFilter(Coupon::STATE_PUBLISHED, $locationCenter, $locationRadius, $categories, $latestCouponsOnly);
        $items = $this->couponRepository->findActiveByCouponLocationFilter($couponLocationFilter);
        $latestItems = $this->couponRepository->findActiveLatestByLocation($locationCenter);

        return $this->handleViewAndSetETag($this->view(['data' => $items, 'included' => ['latestCoupons' => ['data' => $latestItems]]]));
    }

    /**
     * @Rest\Get("/coupon/published/close-by/{location}")
     *
     * @param string $location
     *
     * @throws \InvalidArgumentException
     *
     * @return Response
     */
    public function getPublishedCloseBy(string $location)
    {
        $location = LocationFactory::fromString($location);
        $couponCollection = $this->couponRepository->findActiveOrderByDistance($location);

        return $this->handleView($this->view($this->serializeService->build($couponCollection)));
    }

    public function getAction(Coupon $coupon)
    {
        $redeemState = $this->redeemService->determineRedeemState($coupon, $this->getUser());

        return $this->handleViewAndSetETag($this->view($this->serializeService->buildSingle($coupon, $redeemState)));
    }

    /**
     * @Rest\Post("/coupon/reserve-id")
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function postReserveId()
    {
        $coupon = new Coupon();
        $coupon->setUser($this->getUser());
        $this->entityManager->persist($coupon);
        $this->entityManager->flush();

        return $this->handleView($this->view(['data' => $coupon]));
    }

    public function postAction(Request $request)
    {
        $coupon = new Coupon();
        $coupon->setUser($this->getUser());
        $form = $this->createForm(CouponType::class, $coupon);
        $form->submit($request->request->all());
        if (false === $form->isValid()) {
            return $this->handleView(
                $this->view($form)
            );
        }

        $this->entityManager->persist($form->getData());
        $this->entityManager->flush();

        return $this->handleView($this->view(['data' => $coupon], Response::HTTP_CREATED));
    }

    /**
     * @Rest\Post("/coupon/{coupon}/enable-review")
     *
     * @param Coupon $coupon
     *
     * @throws InsufficientAuthenticationException
     * @throws \LogicException
     *
     * @return Response
     */
    public function postEnableReview(Coupon $coupon)
    {
        $this->denyAccessUnlessGranted(CouponVoter::EDIT, $coupon);

        $this->couponService->enableReview($coupon);

        return $this->handleView($this->view(['data' => $coupon]));
    }

    public function patchAction(Coupon $coupon, Request $request)
    {
        $this->denyAccessUnlessGranted(CouponVoter::EDIT, $coupon);

        $form = $this->createForm(CouponType::class, $coupon);
        $form->submit($request->request->all(), false);
        if (false === $form->isValid()) {
            return $this->handleView($this->view($form));
        }

        $this->entityManager->persist($form->getData());
        $this->entityManager->flush();

        return $this->handleView($this->view(['data' => $coupon]));
    }

    public function deleteAction(Coupon $coupon): Response
    {
        $this->denyAccessUnlessGranted(CouponVoter::DELETE, $coupon);
        $coupon->setState(Coupon::STATE_DELETED);
        $this->entityManager->persist($coupon);
        $this->entityManager->flush();

        return $this->handleView($this->view(null, Response::HTTP_NO_CONTENT));
    }
}
