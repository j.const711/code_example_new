<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Address;
use App\Entity\BillingAddress;
use App\Entity\Store;
use App\Entity\User;
use App\Event\CreatingActiveCustomerEvent;
use App\Exception\User\InvalidUserDataException;
use App\Form\User\BillingAddressType;
use App\Form\User\CreateActiveCustomerType;
use App\Security\Voter\UserVoter;
use App\Service\Api\VersionService;
use App\Service\Store\StoreService;
use App\Service\User\UserService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    /**
     * @var VersionService
     */
    protected $versionService;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var StoreService
     */
    protected $storeService;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(VersionService $versionService, EntityManagerInterface $em, UserService $userService, StoreService $storeService, EventDispatcherInterface $eventDispatcher)
    {
        $this->versionService = $versionService;
        $this->em = $em;
        $this->userService = $userService;
        $this->storeService = $storeService;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getCurrentAction()
    {
        $view = $this->view(['data' => $this->getUser()]);
        $view->getContext()->addGroups(['Default', 'owner']);

        return $this->handleView($view);
    }

    public function getAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::VIEW, $user);

        $view = $this->view(['data' => $user]);
        $view->getContext()->addGroups(['Default', 'owner']);

        return $this->handleView($view);
    }

    /**
     * Endpoints which allows a landing page to create an active customer with a single request
     *
     * @Rest\Post("/user/active-customer")
     *
     * @param Request $request
     *
     * @throws \App\Exception\Payment\InvalidVatIdException
     * @throws \App\Exception\User\AlreadyExistsException
     *
     * @return Response
     */
    public function postActiveCustomerAction(Request $request): Response
    {
        $user = new User();
        $requestData = $request->get('data');
        $form = $this->createForm(CreateActiveCustomerType::class, $user);
        $form->submit($requestData['user']);
        if (false === $form->isValid()) {
            return $this->handleView($this->view(['data' => $form->getErrors()], Response::HTTP_BAD_REQUEST));
        }

        $this->userService->createActiveCustomer($user, $requestData['iban'], $requestData['membershipTypeGroup']);

        $this->eventDispatcher->dispatch(CreatingActiveCustomerEvent::NAME, new CreatingActiveCustomerEvent($user));

        return $this->handleView($this->view(['data' => $user], Response::HTTP_CREATED));
    }

    public function postBillingAddressAction(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

        $billingAddress = $this->createBillingAddress($request->request->get('data'));
        $user->setBillingAddress($billingAddress);

        $this->em->persist($user);
        $this->em->flush();

        return $this->handleView($this->view(['data' => $billingAddress], Response::HTTP_CREATED));
    }

    public function patchCurrentAction(Request $request)
    {
        $user = $this->getUser();
        $fields = $request->get('fields');

        $allowedFields = [
            'company',
            'firstName',
            'lastName',
            'email',
            'description',
            'website',
            'companyPhone',
            'companyEmail',
            'vatId',
            'couponRedeemNotification',
            'dailyCouponRedeemOverview',
            'weeklyCouponRedeemOverview',
            'monthlyCouponRedeemOverview',
        ];

        // billingAddress isn't available until APP_VERSION 1.24.0.
        // We do not touch the billingAddress in this case, to avoid accidental reset to null
        if (array_key_exists('billingAddress', $fields)) {
            $billingAddress = $this->createBillingAddress($fields['billingAddress']);
            $user->setBillingAddress($billingAddress);
        }

        $this->userService->update($user, $fields, $allowedFields);

        if ($this->versionService->isRequestApiVersionSmallerThan($request, VersionService::API_VERSION_5)) {
            // Code for BC with app versions that do not support multiple stores
            $allowedFieldsStore = [
                'street',
                'zip',
                'city',
                'phone',
                'email',
                'hasOpeningTimes',
                'openingTimes',
                'openingTimesComment',
            ];

            if (array_key_exists('companyPhone', $fields)) {
                $fields['phone'] = $fields['companyPhone'];
            }
            if (array_key_exists('companyEmail', $fields)) {
                $fields['email'] = $fields['companyEmail'];
            }

            // Only update / create a store and address, if the user changed one on the store/address fields
            // Otherwise we would create a store for each user, even if just the first/lastname is changed
            if (count(array_intersect_key(array_flip($allowedFieldsStore), $fields)) > 0) {
                $store = $user->getStores()->first();
                if (!$store) {
                    $store = (new Store())
                        ->setAddress(new Address())
                        ->setUser($user);
                }
                $this->storeService->update($store, $fields, $allowedFieldsStore);
            }
        }

        $this->em->refresh($user);

        $view = $this->view(['data' => $user]);
        $view->getContext()->addGroups(['Default', 'owner']);

        return $this->handleView($view);
    }

    /**
     * @Rest\Patch("/user/current/enable-customer-role")
     */
    public function patchCurrentEnableCustomerRole()
    {
        $user = $this->getUser();
        if ($this->userService->allowCustomerRole($user)) {
            $view = $this->view(['data' => $user]);
        } else {
            $view = $this->view(['data' => $user], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * @param array|null $data
     *
     * @throws InvalidUserDataException
     *
     * @return BillingAddress|null
     */
    protected function createBillingAddress(?array $data): ?BillingAddress
    {
        if (empty($data)) {
            return null;
        }

        $billingAddress = new BillingAddress();
        $form = $this->createForm(BillingAddressType::class, $billingAddress);
        $form->submit($data);
        if (false === $form->isValid()) {
            throw new InvalidUserDataException('BillingAddress is invalid');
        }

        $this->em->persist($form->getData());

        return $billingAddress;
    }
}
