<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\EntityTrait\CreatedAndModified;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CouponRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Coupon
{
    use CreatedAndModified;

    /** @var string Coupon was created but has no content */
    public const STATE_CREATED = 'created';
    /** @var string Coupon was created and contains some basic data */
    public const STATE_DRAFT = 'draft';
    /** @var string Coupon is in review process */
    public const STATE_REVIEW = 'review';
    /** @var string Coupon is publicly visible */
    public const STATE_PUBLISHED = 'published';
    /** @var string Coupon is publicly visible */
    public const STATE_DELETED = 'deleted';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title = '';

    /**
     * @ORM\Column(type="text")
     */
    private $description = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="coupons")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Type("Relation")
     */
    private $user;

    /**
     * @ORM\Column(type="text")
     */
    private $legalNotice = '';

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CouponCategory", mappedBy="coupons", cascade={"persist","remove"})
     * @Serializer\Type("Relation<App\Entity\CouponCategory>")
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=20, options={"default" : "created"})
     */
    private $state = self::STATE_CREATED;

    /**
     * @ORM\Column(type="datetime_immutable",nullable=true)
     */
    private $validFrom;

    /**
     * @ORM\Column(type="datetime_immutable",nullable=true)
     */
    private $validUntil;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\CouponRedeemRules", mappedBy="coupon", cascade={"persist", "remove"})
     */
    private $redeemRules;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $publicationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="coupons")
     */
    private $shop;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isOnlineRedeemable = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $onlineRedeemUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $onlineRedeemCode;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLegalNotice(): ?string
    {
        return $this->legalNotice;
    }

    public function setLegalNotice(string $legalNotice): self
    {
        $this->legalNotice = $legalNotice;

        return $this;
    }

    /**
     * @return Collection|CouponCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(CouponCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addCoupon($this);
        }

        return $this;
    }

    public function removeCategory(CouponCategory $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeCoupon($this);
        }

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getValidFrom(): ?DateTimeImmutable
    {
        return $this->validFrom;
    }

    public function setValidFrom(DateTimeImmutable $validFrom): self
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    public function getValidUntil(): ?DateTimeImmutable
    {
        return $this->validUntil;
    }

    public function setValidUntil(DateTimeImmutable $validUntil): self
    {
        $this->validUntil = $validUntil;

        return $this;
    }

    public function getRedeemRules(): ?CouponRedeemRules
    {
        return $this->redeemRules;
    }

    public function setRedeemRules(CouponRedeemRules $redeemRules): self
    {
        $this->redeemRules = $redeemRules;

        // set the owning side of the relation if necessary
        if ($this !== $redeemRules->getCoupon()) {
            $redeemRules->setCoupon($this);
        }

        return $this;
    }

    public function getPublicationDate(): ?DateTimeImmutable
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(?DateTimeImmutable $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function isStatePublished(): bool
    {
        return $this->getState() === static::STATE_PUBLISHED;
    }

    public function isStateReview(): bool
    {
        return $this->getState() === static::STATE_REVIEW;
    }

    public function isStateDraft(): bool
    {
        return $this->getState() === static::STATE_DRAFT;
    }

    public function isStateCreated(): bool
    {
        return $this->getState() === static::STATE_CREATED;
    }

    public function isStatePublishedAndDateActive(): bool
    {
        $now = new DateTimeImmutable();

        return $this->isStatePublished() && $this->validFrom < $now && $now < $this->validUntil;
    }

    public function getIsOnlineRedeemable(): ?bool
    {
        return $this->isOnlineRedeemable;
    }

    public function setIsOnlineRedeemable(bool $isOnlineRedeemable): self
    {
        $this->isOnlineRedeemable = $isOnlineRedeemable;

        return $this;
    }

    public function getOnlineRedeemUrl(): ?string
    {
        return $this->onlineRedeemUrl;
    }

    public function setOnlineRedeemUrl(?string $onlineRedeemUrl): self
    {
        $this->onlineRedeemUrl = $onlineRedeemUrl;

        return $this;
    }

    public function getOnlineRedeemCode(): ?string
    {
        return $this->onlineRedeemCode;
    }

    public function setOnlineRedeemCode(?string $onlineRedeemCode): self
    {
        $this->onlineRedeemCode = $onlineRedeemCode;

        return $this;
    }
}
