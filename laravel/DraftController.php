<?php

namespace App\Http\Controllers;

use App\Draft;
use DateTime;
use hackerESQ\Settings\Facades\Settings;
use http\Exception\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DraftController extends Controller
{
    public function index(Request $request) {
        $user = Auth::user();
        if(!$user->org->moderate) {
            return redirect(route('user-dash'))->with('error', 'Создание заявок станет доступно после модерации организации.');
        }

        $draft = $user->getDraft();
        if($request->method() == 'POST') {
            $draft->step = $request->step;
        }


        switch ($draft->step) {
            case 2:
                return $this->step2($request, $draft);
                break;
            case 3:
                return $this->step3($request, $draft);
                break;
            case 4:
                return $this->step4($request, $draft);
                break;
            default:
                return $this->step1($request, $draft);
                break;
        }

    }

    public function step1(Request $request, Draft $draft) {
        $draft->accepted(false);
        $draft->clearThumbs();
        if($request->method() == 'POST') {
            $draft->save();
            return view('steps.1', compact('draft'));
        } else {
            return view('order.create', compact('draft' ));
        }
    }

    public function step2(Request $request, Draft $draft) {
        if($request->name) $draft->name = $request->name;
        if($request->otprav) $draft->otprav = $request->otprav;
        if($request->fiz_adr) $draft->fiz_adr = $request->fiz_adr;

        if($request->numb_recipients == 'one') {
            $resCheck = $draft->checkQuantityPagesInTamplate();

            if($resCheck !== true) {
                return response()->json(['errorQuantityPages' => $resCheck], 401);
            }

            $dataAdrRecipient = array(
                'first_name' => $request->name_recipient ? $request->name_recipient : '',
                'last_name' => $request->last_name ? $request->last_name : '',
                'middle_name' => $request->middle_name ? $request->middle_name : '',
                'index' => $request->index_recipient ? $request->index_recipient : '',
                'city' => $request->city_recipient ? $request->city_recipient : '',
                'region' => $request->region_recipient ? $request->region_recipient : '',
                'street' => $request->street_recipient ? $request->street_recipient : '',
                'house' => $request->house_recipient ? $request->house_recipient : '',
                'flat' => $request->flat_recipient ? $request->flat_recipient : '',
                'pages' => countPages($draft->getPath().'/input.pdf'),
            );

            $draft->saveAdrRecipient($dataAdrRecipient);
        }

        $count = $draft->getList(true);
        $draft->accepted(false);
        if($request->method() == 'POST') {
            $draft->createBacks($draft->getList());
        }
        $thumb = $draft->getImages();

        if($request->method() == 'POST') {
            $draft->save();
            return view('steps.2', compact('draft', 'thumb', 'count'));
        } else {
            return view('order.create', compact('draft', 'thumb', 'count'));
        }
    }

    public function step3(Request $request, Draft $draft) {
        $list = $draft->getList();
        $count = count($list);
        $tarif = Settings::get('tarif');
        $price = $count * $tarif;
        $thumb = $draft->getImages();
        $draft->accepted();
        if($request->method() == 'POST') {

            $draft->save();
            return view('steps.3', compact('draft', 'thumb', 'count', 'price'));
        } else {
            return view('order.create', compact('draft', 'thumb', 'count', 'price'));
        }
    }


    public function step4(Request $request, Draft $draft) {
        $user = Auth::user();
        if ($user->fiz) {
            if($request->method() == 'POST') {
                $order = $draft->createOrder();
                return view('steps.4-fiz', compact('draft', 'order'));
            } else {
                return view('order.create', compact('draft'));
            }
        } else {
            if($request->method() == 'POST') {
                $order = $draft->createOrder();
                return view('steps.4', compact('draft'));
            } else {
                return view('order.create', compact('draft'));
            }
        }
    }

    public function storeFile(Request $request) {

        $request->validate([
            'mails'=> ['required','mimes:rtf,doc,docx,pdf', 'max:150000']
        ]);

        $draft = $request->user()->getDraft();

        try {
            $draft->storeFile($request->file('mails'), 'file_input');
        } catch (\ErrorException $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }

        return 'ok';

    }

    public function storeTable(Request $request) {

        $request->validate([
            'table'=> ['required','mimetypes:application/zip,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel']
        ]);

        $draft = $request->user()->getDraft();

        try {
            $draft->storeFile($request->file('table'), 'file_list', 'table');
        } catch (\ErrorException $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }

        return ['status'=>'ok'];

    }



    public function template() {
        $path = storage_path('app/docs/templates/template.xlsx');
        return response()->download($path);
    }


    public function thumb(Draft $draft, $file) {

        $user = Auth::user();
        if(!$user) {
            abort('404');
        } elseif (!$user->hasRole(['manager', 'admin'])) {
            if (!File::exists(storage_path('app/drafts/'.$draft->id.'/'.$file)) || $user->id !== $draft->user->id) {
                abort('404');
            }
        }

        return response()
            ->file(storage_path('app/drafts/'.$draft->id.'/'.$file), [
                'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
            ]);
    }


    public function thumbGet(Request $request) {
        $user = Auth::user();
        $draft = $user->getDraft();
        $thumb = $draft->getImages($request->page);
        return $thumb;
    }
}
