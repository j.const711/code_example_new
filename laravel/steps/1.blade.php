<h2 class="userPage__title h3">Размещение заявки в 3 простых шага</h2>
<div class="order">
    <div class="order__row row">
        <div class="col-md-8">
            <div class="order__row">
                <div class="form-group">
                    <label for="nameOrder">Название заявки</label>
                    <input name="name" type="text" value="{{ $draft->name }}" class="validate form-control order__field form-control order__field--short" id="nameOrder" placeholder="Моя заявка" required>
                </div>
            </div>
        </div>
    </div>
    <div class="order__row row">
        <div class="col-md-8">
            @if($user->fiz)
                <div class="order__row">
                    <div class="form-group">
                        <label for="fiz_adr">Адрес отправителя</label>
                        <input name="fiz_adr" type="text" value="{{ $draft->fiz_adr }}" class="validate form-control order__field form-control order__field--short" id="fiz_adr" placeholder="142180, Московская обл., г. Подольск, мкр. Климовск, ул. Индустриальная, д.11" required>
                        <input id="otprav" type="hidden" name="otprav" value="fiz">
                    </div>
                </div>
            @else
                <div class="order__row">
                    <div class="form-group">
                        <label>Отправитель</label>
                        <div class="adr-select">
                            <div class="adr-select__current">
                                @if($draft->otprav)
                                    {{ $draft->otprav_list[$draft->otprav] }}
                                @else
                                    {{ $draft->otprav_list['ur'] }}
                                @endif
                            </div>
                            <div class="adr-select__list">
                                <div class="adr-select__item @if(!$draft->otprav || $draft->otprav == 'ur') active @endif" data-val="ur">
                                    <div class="adr-select__label">
                                        Юридический адрес:
                                    </div>
                                    <div class="adr-select__value">
                                        {{ $draft->otprav_list['ur'] }}
                                    </div>
                                </div>
                                <div class="adr-select__item @if($draft->otprav == 'fact') active @endif" data-val="fact">
                                    <div class="adr-select__label">
                                        Фактический адрес:
                                    </div>
                                    <div class="adr-select__value">
                                        {{ $draft->otprav_list['fact'] }}
                                    </div>
                                </div>
                                <div class="adr-select__item @if($draft->otprav == 'post') active @endif" data-val="post">
                                    <div class="adr-select__label">
                                        Почтовый адрес:
                                    </div>
                                    <div class="adr-select__value">
                                        {{ $draft->otprav_list['post'] }}
                                    </div>
                                </div>
                            </div>
                            @if($draft->otprav)
                                <input id="otprav" type="hidden" name="otprav" value="{{ $draft->otprav }}">
                            @else
                                <input id="otprav" type="hidden" name="otprav" value="ur">
                            @endif

                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-4">
            @if(false)
            <a href="{{ route('create-ny-order') }}" class="ded-tpl-btn">
                Создать новогоднюю заявку
            </a>
            @endif
            <div class="order__desc">
                <div>
                    Необходимо выбрать обратный адрес отправителя
                </div>
            </div>
        </div>
    </div>
    <div class="order__row row ">
        <div class="col-md-8">
            <form method="POST" enctype="multipart/form-data" id="upload_mail" action="javascript:void(0)" >
                <div class="form-group">
                    <label for="mailOrder">Загрузка файла письма</label>
                    <input data-parsley-errors-messages-disabled type="file" name="mails" required class="form-control custom-file-control custom-file-control--short" id="mailOrder">
                    <div id="mailOrderInfo"></div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <div class="order__desc">
                <div>
                    Необходимо загрузить файл готового отправления
                    в формате <span>PDF, DOC, DOCX, RTF</span>
                </div>

            </div>
        </div>
    </div>
    @if($user->fiz)
        <div class="order__row">
            <div class="form-group order__row__number-recipients">
                <label class="mr-sm-2" for="numbRecipients">Количество получателей</label>
                <select class="custom-select mr-sm-2" id="numbRecipients" name="numb_recipients" autocomplete='off'>
                    <option value="one" selected="">Один</option>
                    <option value="several">Несколько</option>
                </select>
            </div>
        </div>
        <div class="order__row order__name-recipient-block row">
            <div class="col-md-8">
                <div class="order__row">
                    <div class="form-group mb-0">
                        <label for="nameRecipient">Имя получателя</label>
                        <input name="name_recipient" type="text" value="" class="form-control order__field form-control order__field--short" id="nameRecipient" disabled>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="order__row">
                    <div class="form-group mb-0">
                        <label for="lastNameRecipient">Фамилия получателя</label>
                        <input name="last_name_recipient" type="text" value="" class="validate form-control order__field form-control order__field--short" id="lastNameRecipient" disabled required>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="order__row">
                    <div class="form-group mb-0">
                        <label for="middleNameRecipient">Отчество получателя</label>
                        <input name="middle_name_recipient" type="text" value="" class="form-control order__field form-control order__field--short" id="middleNameRecipient" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="order__row order__addr-recipient-block row">
            <div class="col-md-8">
                <div class="order__row">
                    <div class="form-group mb-0">
                        <label for="adrRecipient">Адрес получателя</label>
                        <input name="adr_recipient" type="text" value="" class="validate form-control order__field form-control order__field--short" placeholder="142180, Московская обл., г. Подольск, мкр. Климовск, ул. Индустриальная, д.11" id="adrRecipient" disabled required>
                        <div id="adrRecipientInfo"></div>
                        <input id="indexRecipient" type="hidden" name="index_recipient" value="" class="validate" required data-parsley-errors-messages-disabled>
                        <input id="cityRecipient" type="hidden" name="city_recipient" value="" class="validate" required data-parsley-errors-messages-disabled>
                        <input id="regionRecipient" type="hidden" name="region_recipient" value="" class="validate" required data-parsley-errors-messages-disabled>
                        <input id="streetRecipient" type="hidden" name="street_recipient" value="">
                        <input id="houseRecipient" type="hidden" name="house_recipient" value="" class="validate" required data-parsley-errors-messages-disabled>
                        <input id="flatRecipient" type="hidden" name="flat_recipient" value="">
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="order__row order__addr-templ-block @if($user->fiz) order__addr-templ-block_hidden @endif row ">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Шаблон базы рассылки</label>
                        <div class="order__download" id="downloadTempl">
                            <span>Скачать шаблон</span>
                        </div>

                    </div>
                </div>
                <div class="col-md-7">
                    <form method="POST" enctype="multipart/form-data" id="upload_table" action="javascript:void(0)" >
                        <div class="form-group">
                            <label for="dbOrder">Загрузка базы рассылки</label>
                            <input data-parsley-errors-messages-disabled type="file" name="table" disabled required class="validate form-control custom-file-control custom-file-control--short" id="dbOrder">
                            <div id="tableOrderInfo"></div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="order__desc">
                <div>
                    Необходимо заполнить базу рассылки в соответствии с позициями шаблона и загрузить заполненный файл

                </div>
            </div>
        </div>
    </div>
</div>
<div class="orderBottom">
    <div class="row">
        <div class="col-md-2 order-2 order-md-1 mb-md-0 mb-4">
            @if($draft->step > 1)
                <a href="#" class="orderBottom__back btn btn-primary">НАЗАД</a>
            @endif
        </div>
        <div class="col-md-8 order-1 order-md-2 mb-md-0 mb-4">
            @include('steps.steper')
        </div>
        <div class="col-md-2 order-3 order-md-3">
            <a href="#" id="nextPage" onclick="changePage(2, event)" class="orderBottom__next btn btn-primary" style="display: none">ДАЛЕЕ</a>
        </div>
    </div>
</div>
<div class="loader" style="display:none;">
    <div class="loader__img">
        <img src="../img/svg/Loader.svg">
    </div>
    <div class="loader__animate">
        <div class="loader-bubble loader-bubble-white m-2"></div>
    </div>
    <div class="loader__text">
        Ваш макет почти готов!
    </div>
</div>
