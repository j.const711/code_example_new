<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if($_POST['generate-cp']) {
    require_once('generate_cp_pdf.php');
} elseif ($_POST['generate-cp-excel']) {
     require_once('generate_cp_excel.php');  
} elseif($_POST['ajaxRequest']) {
    require_once('recalculation_prices.php');
} elseif($_POST['ajaxRemove']) {
    require_once('remove_item.php');
} elseif($_POST['save-cp']) {
    require_once('save_cp.php');
} else {
    require_once('items_from_basket.php');
}