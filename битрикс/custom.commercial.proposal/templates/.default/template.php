<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$this->addExternalCss($templateFolder . "/summernote/summernote-lite.min.css");
$this->addExternalJS($templateFolder . "/summernote/summernote-lite.min.js");
?>


    <div class="title-and-link-saved-cp-block">
        <div class="title-and-link-saved-cp-block__title">
            КП №<?= $arResult['NUMBER'] ?> <? if(!empty($arResult['ID'])) { ?>(Редактирование)<? } else { ?>(Создание нового)<? } ?>
        </div>
        <? if(isset($arResult['LIST_ALL_SAVED_CP'])
            && !empty($arResult['LIST_ALL_SAVED_CP'])) { ?>
            <div class="title-and-link-saved-cp-block__link">
                <a href="/svoe-cp/saved" class="btn btn-default white">Сохраненные КП</a>
            </div>
        <? } ?>
    </div>
<? if(isset($arResult["ITEMS"])
    && !empty($arResult["ITEMS"])) { ?>
    <div class="bx_ordercart">
        <div id="basket_items_list_for_сp">
            <div class="bx_ordercart_order_table_container">
                <form id="form-svoe-cp" target="_blank" action="/svoe-cp/pdf.php" method="post" autocomplete="off">
                    <?/*?><input type="hidden" name="generate-cp" value="1"><?*/?>
                    <input type="hidden" id="id-cp" name="id-cp" value="<?= $arResult['ID'] ?>">
                    <input type="hidden" id="number-cp" name="number-cp" value="<?= $arResult['NUMBER'] ?>">
                    <input type="hidden" id="total-sum-cp" name="total-sum-cp" value="<?= $arResult['TOTAL_SUM'] ?>">
                    <input type="hidden" id="date-cp" name="date-cp" value="<?= $arResult['CREATED_DATE'] ?>">
                    <table id="basket_items">
                        <thead>
                        <tr>
                            <td class="item" colspan="2" id="col_NAME">
                                Товары
                            </td>
                            <td class="price" id="col_PRICE">
                                Цена Дилерская
                            </td>
                            <td class="price" id="col_PRICE">
                                Цена Розница
                            </td>
                            <td class="custom" id="col_QUANTITY">
                                Количество
                            </td>
                            <td class="custom" id="col_SUM">
                                Сумма
                            </td>
                            <td class="custom" id="col_STATUS">
                                Статус товара
                            </td>
                            <td class="col-del"></td>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($arResult["ITEMS"] as $k => $item): ?>
                            <tr id="productOrder_<?= $item['ID'] ?>" data-id_product="'<?= $item['PRODUCT_ID'] ?>'">
                                <input type="hidden" name="id_basketItem[]" value="<?= $item['PRODUCT_ID'] ?>">
                                <td class="itemphoto">
                                    <img src="<?= $item['IMAGE'] ?>" title="<?= $item['NAME'] ?>" alt="<?= $item['NAME'] ?>">
                                </td>
                                <td class="item">
                                    <strong><?= $item['NAME_WITH_URL'] ?></strong>
                                </td>
                                <td class="price">
                                    <?= $item['DEALER_PRICE'] ?>
                                </td>
                                <td class="price">
                                    <input type="text" name="price_product[<?= $item['PRODUCT_ID'] ?>]" class="price_product" data-id_product="<?= $item['PRODUCT_ID'] ?>" data-old_price="<?= CCurrencyLang::CurrencyFormat($item['PRICE'], $item['CURRENCY']); ?>" value="<?= CCurrencyLang::CurrencyFormat($item['PRICE'], $item['CURRENCY']); ?>">
                                </td>
                                <td class="custom quantity">
                                    <input type="text" name="quantity_product[<?= $item['PRODUCT_ID'] ?>]" class="quantity_product" data-id_product="<?= $item['PRODUCT_ID'] ?>" data-old_quantity="<?= $item['QUANTITY'] . ' ' . $item['MEASURE_NAME'] ?>" value="<?= $item['QUANTITY'] . ' ' . $item['MEASURE_NAME'] ?>">
                                </td>
                                <td class="custom total_sum_product">
                                    <?= CCurrencyLang::CurrencyFormat($item['TOTAL_PRICE'], $item['CURRENCY']); ?>
                                </td>
                                <td class="custom status">
                                    <input type="text" name="status_product[<?= $item['PRODUCT_ID'] ?>]" class="status_product" data-id_product="<?= $item['PRODUCT_ID'] ?>" value="<?= $item['STATUS'] ?>">
                                </td>
                                <td class="remove-cell">
                                    <a data-product_id="<?= $item['ID'] ?>" href="" class="remove js-remove-kp"><i></i></a>
                                    <img class="dragging" width="20px" src="<?=$this->GetFolder()?>/images/brile.png" alt="" style="padding-top: 45px;">
                                </td>
                            </tr>
                        <? endforeach; ?>
                        <tr>
                            <td colspan="7" class="total_sum_order">
                                Итого: <?= CCurrencyLang::CurrencyFormat($arResult['TOTAL_SUM'], 'RUB'); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="div_additional_inf">
                        <label for="additional_inf">Дополнительная информация:</label>
                        <textarea id="additional_inf" name="additional_inf" rows="15" class="35"><?= $arResult['ADDITIONAL_INFORMATION'] ?></textarea>
                    </div>
                    <div class="main-btn-svoe-cp">
                        <button type="button" class="btn btn-success btn-lg main-btn-svoe-cp__save-cp" name="save-cp" value="yes">Сохранить КП</button>
                        <img class="main-btn-svoe-cp__loading-save-cp" src="<?= SITE_TEMPLATE_PATH ?>/images/xloading.gif">
                        <button class="btn btn-default btn-lg main-btn-svoe-cp__generate-cp" name="generate-cp" value="yes">Сформировать КП</button>
                        <button class="btn btn-default btn-lg main-btn-svoe-cp__generate-cp" name="generate-cp-excel" value="yes">Выгрузить КП в Exel</button>
                        
                        
                        <div class="main-btn-svoe-cp__message-success"></div>
                        <div class="main-btn-svoe-cp__message-error"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<? } else { ?>
    <div class="bx_ordercart">
        <div id="basket_items_list_for_сp">
            <div class="bx_ordercart_order_table_container">
                <div>
                    <strong>Список товаров пуст.</strong>
                </div>
            </div>
        </div>
    </div>
<? } ?>