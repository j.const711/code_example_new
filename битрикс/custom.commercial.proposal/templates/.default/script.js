$(function() {
	$('#basket_items_list_for_сp .price_product').focusout(function(){
		var __this = $(this);
		var needProductId = __this.data('id_product');
		var oldPrice = __this.data('old_price');
		var price = __this.val();

		if(price != oldPrice) {
			var arPrices = {};
			var arQuantities = {};

			$("#basket_items_list_for_сp .price_product").each(function() {
				arPrices[$(this).data('id_product')] = $(this).val();
			});

			$("#basket_items_list_for_сp .quantity_product").each(function() {
				arQuantities[$(this).data('id_product')] = $(this).val();
			});

			$.post("/svoe-cp/", {ajaxRequest: 'yes', needProductId: needProductId, arPrices: arPrices, arQuantities: arQuantities}, function(data) {
				if(data.status == 'ok') {
					$('#basket_items_list_for_сp .total_sum_order').html('Итого: ' + data.totalSumOrderWithMeasure);
					$('#form-svoe-cp #total-sum-cp').val(data.totalSumOrder);
					__this.parents().eq(1).find('.total_sum_product').html(data.totalSumProduct);
					__this.data('old_price', price);
				} else {
					console.log('ошибка');
				}
			}, "json");
		}
	});

	$('#basket_items_list_for_сp .quantity_product').focusout(function(){
		var __this = $(this);
		var needProductId = __this.data('id_product');
		var oldQuantity = __this.data('old_quantity');
		var quantity = __this.val();

		if(quantity != oldQuantity) {
			var arPrices = {};
			var arQuantities = {};

			$("#basket_items_list_for_сp .price_product").each(function() {
				arPrices[$(this).data('id_product')] = $(this).val();
			});

			$("#basket_items_list_for_сp .quantity_product").each(function() {
				arQuantities[$(this).data('id_product')] = $(this).val();
			});

			$.post("/svoe-cp/", {ajaxRequest: 'yes', needProductId: needProductId, arPrices: arPrices, arQuantities: arQuantities}, function(data) {
				if(data.status == 'ok') {
					$('#basket_items_list_for_сp .total_sum_order').html('Итого: ' + data.totalSumOrderWithMeasure);
					$('#form-svoe-cp #total-sum-cp').val(data.totalSumOrder);
					__this.parents().eq(1).find('.total_sum_product').html(data.totalSumProduct);
					__this.data('old_quantity', quantity);
				} else {
					console.log('ошибка');
				}
			}, "json");
		}
	});

	function makeMoney(n) {
		return parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ");
	}

	$("#basket_items_list_for_сp").on('click', '.js-remove-kp', function () {
		let removeId = $(this).data('product_id');
		$('#productOrder_'+removeId).remove();
		$.post("/svoe-cp/", {ajaxRemove: 'yes', removeProductId: removeId}, function(data) {}, "json");
		let newSumma = 0;
		if($("#basket_items_list_for_сp .total_sum_product").length > 0) {
			$("#basket_items_list_for_сp .total_sum_product").each(function() {
				newSumma = newSumma + parseFloat($(this).html().replace(' руб.', '').replace(' ', ''));
			});
			$('.total_sum_order').html('Итого: '+ makeMoney(newSumma) +' руб.');
		} else {
			$('#bx_ordercart_order_table_container').html('<div><strong>Список товаров пуст.</strong></div>');
		}
		return false;
	});

	$('#additional_inf').summernote({
		height: 300,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'underline', 'italic']],
			['color', ['color']],
			['view', ['fullscreen', 'codeview', 'help']]
		]
	});

	/* Код сохраения КП */
	$("button[name='save-cp']").click(function() {
		var __this = $(this);
		var prodData = {};
		var id = $('#form-svoe-cp').find('#id-cp').val();
		var numberCp = $('#form-svoe-cp').find('#number-cp').val();
		var additionalInf = $('#form-svoe-cp').find('#additional_inf').val();
		var totalSum = $('#form-svoe-cp').find('#total-sum-cp').val();

		__this.toggle();
		$('.main-btn-svoe-cp__loading-save-cp').toggle();

		$('tr[id^="productOrder_"]').each(function() {
			prodData[$(this).data('id_product')] = {'price': $(this).find('.price_product').val(), 'quantity': $(this).find('.quantity_product').val(), 'status': $(this).find('.status_product').val()};
		});

		if(Object.keys(prodData).length > 0) {
			$.post("/svoe-cp/", {'save-cp': 'yes', 'id': id, 'number-cp': numberCp, 'prodData': prodData, 'additional_inf': additionalInf, 'total_sum': totalSum}, function (data) {
				if(data.status == 'ok') {
					if(window.location.href.indexOf("cp_id") == -1) {
						$('#form-svoe-cp').find('#id-cp').val(data.new_cp_id);

						if(window.location.href.indexOf("?") > -1) {
							var newurl = document.location.href+"&cp_id="+data.new_cp_id;
						}else{
							var newurl = document.location.href+"?cp_id="+data.new_cp_id;
						}

						window.history.pushState({ path: newurl }, '', newurl);
					}

					$('.main-btn-svoe-cp__message-success').html(data.message).show(1).delay(5000).hide(1);
				} else {
					$('.main-btn-svoe-cp__message-error').html(data.message).show(1).delay(5000).hide(1);
				}

				__this.toggle();
				$('.main-btn-svoe-cp__loading-save-cp').toggle();
			}, "json");
		} else {
			__this.toggle();
			$('.main-btn-svoe-cp__loading-save-cp').toggle();

			$('.main-btn-svoe-cp__message-success').hide();
			$('.main-btn-svoe-cp__message-error').html('Не удалось сохранить КП').show(1).delay(5000).hide(1);
		}
	});

	let dragBtn = document.querySelector('.dragging');
	$('.dragging').on('mousedown', function () {
		let _this = $(this);
		document.querySelectorAll('tr[id^="productOrder_"]').forEach(e => {
			let dragBlock = e;
			e.draggable = true;
			_this.attr('draggable', false);

			e.ondragstart = e => {
				e.dataTransfer.setData("id", e.target.id);
				e.target.classList.add('dragging');
			};

			e.ondragover = e => {
				let old = document.querySelector('.over');
				old && old.classList.remove('over')
				e.target.classList.add('over');
				e.preventDefault();
			};

			e.ondrop = e => {
				let old = document.querySelector('.dragging');
				let fromEl = document.querySelector('#' + e.dataTransfer.getData('id'));
				let result;

				old && old.classList.remove('dragging')
				old = document.querySelector('.over');
				old && old.classList.remove('over');

				result = $(e.target).parents('tr[id^="productOrder_"]');

				result.before(fromEl);
			};

			e.ondragend = e => {
				document.querySelectorAll('tr[id^="productOrder_"]').forEach(ob => {
					ob.draggable = false;
				});
			};

		});
	});
});